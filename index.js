// The "fetch" method will return a "promise" that resolves to "response" object
fetch('https://jsonplaceholder.typicode.com/posts')
// The first ".then" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
.then((response) => response.json())
// The second ".then" method prints the converted JSON value from the "fetch" request
.then((data) => showPosts(data));

// ADD POST DATA
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	// Prevents the page from reloading
	// Prevents default behavior of event
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		// JSON.stringify converts the object data into stringified JSON
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {'Content-Type': "application/json; charset=UTF-8"}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully added!');

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null
	})

});

// RETRIEVE POSTS
const showPosts = (posts) => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
			<div id = "post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id='post-body-${post.id}'>${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

	// console.log(postEntries);
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// EDIT POST

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
	// To remove the "disabled" attribute from the update button
	// removeAttribute() - removes the attribute with the specified name from the element
	document.querySelector("#btn-submit-update").removeAttribute('disabled')
}

// UPDATE POST

document.querySelector("#form-edit-post").addEventListener('submit', (e) => {
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {'Content-Type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully updated!')

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		// .setAttribute - sets an attribute to an HTML element
		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
	})
});

// DELETE POST

const deletePost = (id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: 'DELETE'
	})

	document.querySelector(`#post-${id}`).remove();
}